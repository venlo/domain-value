/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainDecimal;
import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.DecimalMeta;
import io.venlo.domain.value.meta.RoundingMeta;

public class DocDomainDecimalTest {

	class Amount extends DomainDecimal<Amount> {

		public Amount(final BigDecimal value) {
			super(DecimalMeta.create(5, 2, RoundingMeta.HALF_UP), value);
		}

		public Amount(final DomainDecimal<?> value) {
			this(value.getValue());
		}

		@Override
		protected Amount createInstance(final BigDecimal value) {
			return new Amount(value);
		}
	}

	class Quantity extends DomainDecimal<Quantity> {

		public Quantity(final BigDecimal value) {
			super(DecimalMeta.create(5, 2, RoundingMeta.HALF_UP), value);
		}

		@Override
		protected Quantity createInstance(final BigDecimal value) {
			return new Quantity(value);
		}
	}

	class Price extends DomainDecimal<Price> {

		public Price(final BigDecimal value) {
			super(DecimalMeta.create(5, 3, RoundingMeta.HALF_UP), value);

			if (isNegative()) {
				throw DomainValueException.create("Negative price not allowed: " + this);
			}
		}

		@Override
		protected Price createInstance(final BigDecimal value) {
			return new Price(value);
		}

		public Amount multiply(final Quantity quantity) {
			return new Amount(super.multiply(quantity));
		}
	}

	@Test
	public void scaleTest() {
		System.out.println("Scale:");
		System.out.println("- increase from 0   to 0.00    : " + new Amount(BigDecimal.ZERO));
		System.out.println("- increase from 1   to 1.00    : " + new Amount(new BigDecimal("1")));
		System.out.println("- increase from 1.2 to 1.20    : " + new Amount(new BigDecimal("1.2")));
		System.out.println("- valid amount 1.23            : " + new Amount(new BigDecimal("1.23")));
		System.out.println("- round down from 1.233 to 1.23: " + new Amount(new BigDecimal("1.233")));
		System.out.println("- round up   from 1.237 to 1.24: " + new Amount(new BigDecimal("1.237")));
	}

	@Test
	public void precisionTest() {
		System.out.println("Precision:");
		System.out.println("- decrease from 12345.67  to     12346.00 : " + new Amount(new BigDecimal("12345.67")));
		System.out.println("- decrease from 123456789 to 123460000.00 : " + new Amount(new BigDecimal("123456789")));
	}

	@Test
	public void amountCalculation() {

		final Price price = new Price(new BigDecimal("13.3329"));
		final Quantity quantity = new Quantity(new BigDecimal("3.50"));

		final Amount amount = price.multiply(quantity);

		System.out.println("Price      " + price);
		System.out.println("Quantity * " + quantity);
		System.out.println("Amount   = " + amount);
	}

	@Test
	public void priceValidation() {
		assertThrows(DomainValueException.class, () -> {
			final Price price = new Price(new BigDecimal("-3.59"));
		});
	}

	@Test
	public void convenienceMethods() {

		final Amount amount = new Amount(new BigDecimal("1.23"));

		System.out.println("absolute    : " + amount.absolute());
		System.out.println("negate      : " + amount.negate());
		System.out.println("is-negative : " + amount.isNegative());
		System.out.println("is-positive : " + amount.isPositive());
		System.out.println("is-zero     : " + amount.isZero());
		System.out.println("is-non-zero : " + amount.isNonZero());
		System.out.println("is-one      : " + amount.isOne());
		System.out.println("is-minus one: " + amount.isNegativeOne());

	}
}
