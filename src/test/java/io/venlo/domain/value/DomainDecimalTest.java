/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainDecimal;
import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.DecimalMeta;
import io.venlo.domain.value.meta.RoundingMeta;

public class DomainDecimalTest {

	class Amount extends DomainDecimal<Amount> {

		public Amount(final BigDecimal value) {

			super(DecimalMeta.create(10, 2, RoundingMeta.HALF_UP), value);
		}

		@Override
		protected Amount createInstance(final BigDecimal value) {
			return new Amount(value);
		}
	}

	/*
	 * Rounding
	 */
	@Test
	public void roundingNoneTest() {
		final Amount a1 = new Amount(new BigDecimal("123.45"));
		final Amount a2 = new Amount(new BigDecimal("123.4500"));

		final Amount a3 = new Amount(new BigDecimal("-123.45"));
		final Amount a4 = new Amount(new BigDecimal("-123.4500"));

		assertEquals(a1, a2);
		assertEquals(a3, a4);
	}

	@Test
	public void roundingUpTest() {
		final Amount a1 = new Amount(new BigDecimal("123.4440")); // No
		final Amount a2 = new Amount(new BigDecimal("123.4449")); // No
		final Amount a3 = new Amount(new BigDecimal("123.4450")); // Yes

		final Amount r1 = new Amount(new BigDecimal("123.45"));

		assertNotEquals(r1, a1);
		assertNotEquals(r1, a2);

		assertEquals(r1, a3);
	}

	@Test
	public void roundingNegativeUpTest() {
		final Amount a1 = new Amount(new BigDecimal("-123.4440")); // No
		final Amount a2 = new Amount(new BigDecimal("-123.4449")); // No
		final Amount a3 = new Amount(new BigDecimal("-123.4450")); // Yes

		final Amount r1 = new Amount(new BigDecimal("-123.45"));

		assertNotEquals(r1, a1);
		assertNotEquals(r1, a2);

		assertEquals(r1, a3);
	}

	@Test
	public void roundingDownTest() {

		final Amount a1 = new Amount(new BigDecimal("123.4550")); // No
		final Amount a2 = new Amount(new BigDecimal("123.4549")); // Yes
		final Amount a3 = new Amount(new BigDecimal("123.4540")); // Yes

		final Amount r1 = new Amount(new BigDecimal("123.45"));

		assertNotEquals(r1, a1);

		assertEquals(r1, a2);
		assertEquals(r1, a3);
	}

	@Test
	public void roundingNegativeDownTest() {

		final Amount a1 = new Amount(new BigDecimal("-123.4550")); // No
		final Amount a2 = new Amount(new BigDecimal("-123.4549")); // Yes
		final Amount a3 = new Amount(new BigDecimal("-123.4540")); // Yes

		final Amount r1 = new Amount(new BigDecimal("-123.45"));

		assertNotEquals(r1, a1);

		assertEquals(r1, a2);
		assertEquals(r1, a3);
	}

	/*
	 * Equals
	 */
	@Test
	public void equalsPositiveTest() {
		final Amount a1 = new Amount(BigDecimal.ZERO);
		final Amount a2 = new Amount(new BigDecimal("12.3"));
		final Amount a3 = new Amount(new BigDecimal("12.30"));
		final Amount a4 = new Amount(new BigDecimal("-12.3"));
		final Amount a5 = new Amount(new BigDecimal("12.34"));
		final Amount a6 = new Amount(new BigDecimal("12.35"));
		final Amount a7 = new Amount(new BigDecimal("12.30"));

		assertEquals(a1, a1);
		assertEquals(a2, a2);
		assertEquals(a3, a3);
		assertEquals(a4, a4);
		assertEquals(a5, a5);
		assertEquals(a6, a6);

		assertEquals(a2, a3);
		assertEquals(a2, a7);

		assertNotEquals(a1, a2);
		assertNotEquals(a2, a4);
		assertNotEquals(a2, a5);
		assertNotEquals(a2, a6);

		assertNotEquals(a3, a4);
		assertNotEquals(a3, a5);
		assertNotEquals(a3, a6);

		assertNotEquals(a4, a5);
		assertNotEquals(a4, a6);

		assertNotEquals(a5, a6);
	}

	@Test
	public void equalsNegativeTest() {
		final Amount a1 = new Amount(new BigDecimal("55"));
		final Amount a2 = new Amount(new BigDecimal("55.0"));
		final Amount a3 = new Amount(new BigDecimal("55.00"));

		final Amount a4 = new Amount(new BigDecimal("-55"));
		final Amount a5 = new Amount(new BigDecimal("-55.0"));
		final Amount a6 = new Amount(new BigDecimal("-55.00"));

		assertEquals(a3, a1);
		assertEquals(a3, a2);
		assertEquals(a3, a3);

		assertEquals(a4, a4);
		assertEquals(a4, a5);
		assertEquals(a4, a6);

		assertNotEquals(a3, a4);
		assertNotEquals(a3, a5);
		assertNotEquals(a3, a6);
	}

	/*
	 * Negate
	 */
	@Test
	public void negateZeroTest() {
		final Amount a1 = new Amount(BigDecimal.ZERO);
		assertEquals(a1, a1.negate());
	}

	public void negateNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("12.34"));
		final Amount a2 = new Amount(new BigDecimal("-12.34"));

		assertEquals(a1, a2.negate());
		assertEquals(a2, a1.negate());

		assertEquals(a1, a1.negate().negate());
		assertEquals(a2, a2.negate().negate());
	}

	/*
	 * Absolute
	 */
	@Test
	public void absoluteZeroTest() {
		final Amount a1 = new Amount(BigDecimal.ZERO);
		assertEquals(a1, a1.absolute());
	}

	@Test
	public void absoluteNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("12.34"));
		final Amount a2 = new Amount(new BigDecimal("-12.34"));

		assertEquals(a1, a1.absolute());
		assertEquals(a1, a2.absolute());

		assertNotEquals(a2, a2.absolute());
	}

	/*
	 * Add / subtract
	 */
	@Test
	public void addSubtractZeroTest() {
		final Amount a1 = new Amount(BigDecimal.ZERO);
		final Amount a2 = new Amount(new BigDecimal("12.34"));
		final Amount a3 = new Amount(new BigDecimal("-12.34"));

		// Add
		assertEquals(a1, a1.add(a1));
		assertEquals(a2, a1.add(a2));
		assertEquals(a3, a1.add(a3));

		assertEquals(a1, a2.add(a3));
		assertEquals(a1, a3.add(a2));

		// Subtract
		assertEquals(a1, a1.subtract(a1));
		assertEquals(a3, a1.subtract(a2));
		assertEquals(a2, a1.subtract(a3));

		assertEquals(a1, a2.subtract(a2));
		assertEquals(a1, a3.subtract(a3));
	}

	@Test
	public void addSubtractNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("2.12"));
		final Amount a2 = new Amount(new BigDecimal("12.34"));
		final Amount a3 = new Amount(new BigDecimal("14.46"));

		assertEquals(a3, a1.add(a2));
		assertEquals(a1, a3.subtract(a2));
	}

	/*
	 * Multiply
	 */
	@Test
	public void multiplyZeroTest() {
		final Amount a1 = new Amount(BigDecimal.ZERO);
		final Amount a2 = new Amount(BigDecimal.ONE);
		final Amount a3 = new Amount(new BigDecimal("12.34"));
		final Amount a4 = new Amount(new BigDecimal("-5.75"));

		assertEquals(a1, a1.multiply(a1));
		assertEquals(a1, a2.multiply(a1));
		assertEquals(a1, a3.multiply(a1));
		assertEquals(a1, a4.multiply(a1));

		assertEquals(a1, a1.multiply(a2));
		assertEquals(a2, a2.multiply(a2));
		assertEquals(a3, a3.multiply(a2));
		assertEquals(a4, a4.multiply(a2));
	}

	@Test
	public void multiplyNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("12.34"));
		final Amount a2 = new Amount(new BigDecimal("5.75"));
		final Amount a3 = new Amount(new BigDecimal("5.76"));

		final Amount a4 = new Amount(new BigDecimal("70.96"));
		final Amount a5 = new Amount(new BigDecimal("71.08"));

		final Amount a6 = new Amount(new BigDecimal("5043.84"));

		assertEquals(a4, a1.multiply(a2)); // 70.955
		assertEquals(a5, a1.multiply(a3)); // 71.078

		assertEquals(a6, a4.multiply(a5)); // 5043,837
	}

	/*
	 * Division
	 */
	@Test
	public void divideZeroTest() {

		final Amount a1 = new Amount(BigDecimal.ZERO);
		final Amount a2 = new Amount(new BigDecimal("12.34"));

		assertThrows(ArithmeticException.class, () -> {
			a2.divide(a1);
		});

	}

	@Test
	public void divideOneTest() {

		final Amount a1 = new Amount(BigDecimal.ONE);
		final Amount a2 = new Amount(new BigDecimal("12.34"));
		final Amount a3 = new Amount(new BigDecimal("-5.75"));

		assertEquals(a1, a1.divide(a1));
		assertEquals(a2, a2.divide(a1));
		assertEquals(a3, a3.divide(a1));
	}

	@Test
	public void devideNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("12.34"));
		final Amount a2 = new Amount(new BigDecimal("5.75"));
		final Amount a3 = new Amount(new BigDecimal("5.76"));

		final Amount a4 = new Amount(new BigDecimal("2.15"));
		final Amount a5 = new Amount(new BigDecimal("2.14"));

		final Amount a6 = new Amount(new BigDecimal("1.00"));

		assertEquals(a4, a1.divide(a2)); // 2,146
		assertEquals(a5, a1.divide(a3)); // 2,142

		assertEquals(a6, a4.divide(a5)); // 1,0046
	}

	/*
	 * Percentage
	 */
	@Test
	public void percentageNegativeTest() {
		final Amount a1 = new Amount(new BigDecimal("-10"));
		final Amount a2 = new Amount(new BigDecimal("123.45"));

		assertThrows(DomainValueException.class, () -> {
			a2.percentage(a1);
		});
	}

	@Test
	public void percentageZeroTest() {
		final Amount zero = new Amount(BigDecimal.ZERO);

		final Amount a2 = new Amount(new BigDecimal("-1245.3"));
		final Amount a3 = new Amount(new BigDecimal("123.45"));

		assertEquals(zero, zero.percentage(zero));
		assertEquals(zero, a2.percentage(zero));
		assertEquals(zero, a3.percentage(zero));
	}

	@Test
	public void percentageHunderdTest() {
		final Amount hundred = new Amount(new BigDecimal("100"));

		final Amount a2 = new Amount(new BigDecimal("-1245.3"));
		final Amount a3 = new Amount(new BigDecimal("123.45"));

		assertEquals(hundred, hundred.percentage(hundred));
		assertEquals(a2, a2.percentage(hundred));
		assertEquals(a3, a3.percentage(hundred));
	}

	@Test
	public void percentageNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("123.45"));
		final Amount a2 = new Amount(new BigDecimal("123.50"));

		final Amount onePercent = new Amount(new BigDecimal("1"));
		final Amount tenPercent = new Amount(new BigDecimal("10"));

		final Amount a5 = new Amount(new BigDecimal("1.23"));
		final Amount a6 = new Amount(new BigDecimal("1.24"));

		final Amount a7 = new Amount(new BigDecimal("12.35"));

		assertEquals(a5, a1.percentage(onePercent));
		assertEquals(a6, a2.percentage(onePercent));

		assertEquals(a7, a1.percentage(tenPercent));
		assertEquals(a7, a2.percentage(tenPercent));
	}

	@Test
	public void percentageNegativeNonZeroTest() {
		final Amount a1 = new Amount(new BigDecimal("-123.45"));
		final Amount a2 = new Amount(new BigDecimal("-123.50"));

		final Amount onePercent = new Amount(new BigDecimal("1"));
		final Amount tenPercent = new Amount(new BigDecimal("10"));

		final Amount a5 = new Amount(new BigDecimal("-1.23"));
		final Amount a6 = new Amount(new BigDecimal("-1.24"));

		final Amount a7 = new Amount(new BigDecimal("-12.35"));

		assertEquals(a5, a1.percentage(onePercent));
		assertEquals(a6, a2.percentage(onePercent));

		assertEquals(a7, a1.percentage(tenPercent));
		assertEquals(a7, a2.percentage(tenPercent));
	}

	/*
	 * Min / Max
	 */
	@Test
	public void minMaxTest() {
		final Amount a1 = new Amount(new BigDecimal("-1245.3"));
		final Amount a2 = new Amount(new BigDecimal("123.45"));

		final Amount a3 = new Amount(new BigDecimal("541.45"));
		final Amount a4 = new Amount(new BigDecimal("541.46"));

		/*
		 * Min
		 */
		assertEquals(a1, a1.min(a1));
		assertEquals(a2, a2.min(a2));

		assertEquals(a1, a1.min(a2));
		assertEquals(a1, a2.min(a1));

		assertEquals(a2, a2.min(a3));
		assertEquals(a2, a2.min(a4));

		assertEquals(a3, a3.min(a4));

		/*
		 * Max
		 */
		assertEquals(a1, a1.max(a1));
		assertEquals(a2, a2.max(a2));

		assertEquals(a2, a1.max(a2));
		assertEquals(a2, a2.max(a1));

		assertEquals(a3, a2.max(a3));
		assertEquals(a4, a2.max(a4));

		assertEquals(a3, a2.max(a3));
		assertEquals(a4, a2.max(a4));

		assertEquals(a4, a3.max(a4));
	}

}
