/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.base.BaseBoolean;

public class BaseBooleanTest {

	class OrderConfirmed extends BaseBoolean {

		public OrderConfirmed(final Boolean value) {
			super(value);
		}
	}

	@Test
	public void testTrue() {
		final OrderConfirmed b1 = new OrderConfirmed(true);
		assertTrue(b1.isTrue());
		assertFalse(b1.isFalse());
	}

	@Test
	public void testFalse() {
		final OrderConfirmed b1 = new OrderConfirmed(false);
		assertTrue(b1.isFalse());
		assertFalse(b1.isTrue());
	}

	@Test
	public void testFalseEquals() {
		final OrderConfirmed b1 = new OrderConfirmed(false);
		final OrderConfirmed b2 = new OrderConfirmed(false);
		assertTrue(b1.equals(b2));
	}

	@Test
	public void testTrueEquals() {
		final OrderConfirmed b1 = new OrderConfirmed(true);
		final OrderConfirmed b2 = new OrderConfirmed(true);
		assertTrue(b1.equals(b2));
	}

	@Test
	public void testFalseNotEqualsTrue() {
		final OrderConfirmed b1 = new OrderConfirmed(false);
		final OrderConfirmed b2 = new OrderConfirmed(true);
		assertFalse(b1.equals(b2));
		assertFalse(b2.equals(b1));
	}

}
