/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainEnumerate;
import io.venlo.domain.value.EnumerateItem;
import io.venlo.domain.value.meta.EnumerateMeta;

public class BaseEnumerateTest {

	enum YesNoCancel implements DomainEnumerate {
		YES(EnumerateItem.create("YES", "Y","yesLabelCode")),
		NO(EnumerateItem.create("NO", "N","noLabelCode")),
		OPTIONAL(EnumerateItem.create("OPTIONAL", "O","optionalLabelCode"));

		private static EnumerateMeta meta = EnumerateMeta.create(
				YES.item,
				NO.item,
				OPTIONAL.item);

		private final EnumerateItem item;

		private YesNoCancel(final EnumerateItem item) {
			this.item = item;
		}

		@Override
		public EnumerateItem getValue() {
			return item;
		}

		public static EnumerateMeta meta() {
			return meta;
		}

		@Override
		public String getCode() {
			return item.getCode();
		}
	}

	@Test
	public void testGetValue() {
		assertEquals("Y", YesNoCancel.YES.getCode());
		assertEquals("N", YesNoCancel.NO.getCode());
		assertEquals("O", YesNoCancel.OPTIONAL.getCode());
	}

	@Test
	public void testValueOf() {
		assertEquals(YesNoCancel.YES, YesNoCancel.valueOf("YES"));
		assertEquals(YesNoCancel.NO, YesNoCancel.valueOf("NO"));
		assertEquals(YesNoCancel.OPTIONAL, YesNoCancel.valueOf("OPTIONAL"));
	}


	@Test
	public void testValueOfCode() {
		assertEquals(YesNoCancel.YES.item, YesNoCancel.meta().valueOfCode("Y"));
		assertEquals(YesNoCancel.NO.item, YesNoCancel.meta().valueOfCode("N"));
		assertEquals(YesNoCancel.OPTIONAL.item, YesNoCancel.meta().valueOfCode("O"));
	}
}
