/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.base.BaseDate;

public class BaseDateTest {

	class TestDate extends BaseDate {

		public TestDate(final LocalDate value) {
			super(value);
		}
	}

	@Test
	public void dateTest() {
		final TestDate f1 = new TestDate(LocalDate.of(2018, 03, 01));
		final TestDate f2 = new TestDate(LocalDate.of(2018, 03, 01));

		final TestDate t1 = new TestDate(LocalDate.of(2018, 03, 02));
		final TestDate t2 = new TestDate(LocalDate.of(2018, 03, 02));

		assertEquals(f1, f1);
		assertEquals(f1, f2);

		assertEquals(t1, t1);
		assertEquals(t1, t2);

		assertNotEquals(f1, t1);
		assertNotEquals(f1, t2);
	}
}
