/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainDate;

public class DocDomainDateTest {

	class InvoiceDate extends DomainDate<InvoiceDate> {

		public InvoiceDate(final LocalDate value) {
			super(value);
		}

		@Override
		protected InvoiceDate createInstance(final LocalDate value) {
			return new InvoiceDate(value);
		}
	}

	@Test
	public void test() {

		final InvoiceDate d1 = new InvoiceDate(LocalDate.now());
		final InvoiceDate d2 = d1.plusDays(5);
		final InvoiceDate d3 = d1.minusDays(5);

		System.out.println("d1.plusDays(5) : " + d2);
		System.out.println("d1.minusDays(5): " + d3);

		System.out.println("d2.isBefore(d1): " + d2.isBefore(d1));
		System.out.println("d2.isAfter(d1) : " + d2.isAfter(d1));
		System.out.println("d2.equals(d1)  : " + d2.equals(d1));
	}
}