package io.venlo.domain.value.converter;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.converter.DateConverter;
import io.venlo.domain.value.meta.DateMeta;

public class DateConverterTest {

	DateConverter converter = DateConverter.create(DateMeta.create());

	@Test
	public void validateDateConversion() {

		ConverterTestUtil.assertInversible(converter, LocalDate.of(2017, 12, 31));
		ConverterTestUtil.assertInversible(converter, LocalDate.of(2018, 01, 01));
	}
}
