package io.venlo.domain.value.converter;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.converter.DateTimeConverter;
import io.venlo.domain.value.meta.DateTimeMeta;
import io.venlo.domain.value.meta.TimePrecisionMeta;

public class DateTimeConverterTest {

	DateTimeConverter converter = DateTimeConverter.create(DateTimeMeta.create(TimePrecisionMeta.SECONDS));

	@Test
	public void validateDateConversion() {

		ConverterTestUtil.assertInversible(converter, LocalDateTime.of(2017, 12, 31, 10,00));
		ConverterTestUtil.assertInversible(converter, LocalDateTime.of(2018, 01, 01, 8,00));
	}
}
