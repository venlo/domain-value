package io.venlo.domain.value.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.converter.ValueConverter;

public class ConverterTestUtil {

	public static <T> void assertInversible(final ValueConverter<T> converter, @Nullable final T fromDate) {

		final String toString = converter.toString(fromDate);
		@Nullable
		final T toDate = converter.fromString(toString);

		if (fromDate == null) {
			assertNull(toDate);
		} else {
			assertEquals(fromDate, toDate);
		}
	}
}
