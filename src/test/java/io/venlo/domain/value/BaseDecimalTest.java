/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.base.BaseDecimal;
import io.venlo.domain.value.meta.DecimalMeta;
import io.venlo.domain.value.meta.RoundingMeta;

public class BaseDecimalTest {

	private static final DecimalMeta meta = DecimalMeta.create(5, 2, RoundingMeta.HALF_DOWN);

	class Amount extends BaseDecimal {

		public Amount(final BigDecimal value) {
			super(meta, value);
		}
	}

	@Test
	public void amountZeroTest() {

		final Amount a1 = new Amount(BigDecimal.ZERO);
		final Amount a2 = new Amount(new BigDecimal("0"));
		final Amount a3 = new Amount(new BigDecimal("0.0"));
		final Amount a4 = new Amount(new BigDecimal("0.00"));

		assertEquals(a1, a2);
		assertEquals(a1, a3);
		assertEquals(a1, a4);

		assertTrue(a1.isZero());
		assertTrue(a2.isZero());
		assertTrue(a3.isZero());
		assertTrue(a4.isZero());

		assertFalse(a1.isNegative());
		assertFalse(a1.isPositive());
	}

	@Test
	public void amountOneTest() {

		final Amount a1 = new Amount(BigDecimal.ONE);
		final Amount a2 = new Amount(new BigDecimal("1"));
		final Amount a3 = new Amount(new BigDecimal("1.0"));
		final Amount a4 = new Amount(new BigDecimal("1.00"));

		assertEquals(a1, a2);
		assertEquals(a1, a3);
		assertEquals(a1, a4);

		assertTrue(a1.isNonZero());
		assertTrue(a2.isNonZero());
		assertTrue(a3.isNonZero());
		assertTrue(a4.isNonZero());

		assertTrue(a1.isOne());
		assertTrue(a2.isOne());
		assertTrue(a3.isOne());
		assertTrue(a4.isOne());

		assertFalse(a1.isNegative());
		assertTrue(a1.isPositive());
	}

	@Test
	public void amountNonZeroTest() {

		final Amount a1 = new Amount(new BigDecimal("0.00"));
		final Amount a2 = new Amount(new BigDecimal("0.01"));
		final Amount a3 = new Amount(new BigDecimal("-0.01"));

		assertNotEquals(a1, a2);
		assertNotEquals(a1, a3);

		assertTrue(a2.isNonZero());
		assertTrue(a3.isNonZero());

		assertTrue(a2.isPositive());
		assertTrue(a3.isNegative());

		assertFalse(a2.isNegative());
		assertFalse(a3.isPositive());
	}
}
