/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainString;
import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.StringMeta;

public class DocDomainStringTest {

	class Description extends DomainString<Description> {

		public Description(final String value) {
			super(StringMeta.create(60, false), value);
		}

		@Override
		protected Description createInstance(final String value) {
			return new Description(value);
		}
	}

	@Test
	public void test1() {

		final Description d1 = new Description("abcdefg");

		System.out.println("d1.append('_hij'): " + d1.append("_hij"));
		System.out.println("getLeft(3)       : " + d1.getLeft(3));
		System.out.println("isEmpty()        : " + d1.isEmpty());
		System.out.println("isNotEmpty()     : " + d1.isNotEmpty());
	}

	@Test
	public void test2() {
		assertThrows(DomainValueException.class, () -> {
			new Description("abcdefg".repeat(30));
		});
	}
}