/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainDecimal;
import io.venlo.domain.value.meta.DecimalMeta;
import io.venlo.domain.value.meta.RoundingMeta;

public class DomainDecimalTest2 {

	final DecimalMeta meta = DecimalMeta.create(10, 2, RoundingMeta.HALF_UP);

	class Amount extends DomainDecimal<Amount> {

		public Amount(final BigDecimal value) {
			super(meta, value);
		}

		public Amount(final double doubleValue) {
			super(meta, doubleValue);
		}

		public Amount(final String stringValue) {
			super(meta, stringValue);
		}

		@Override
		protected Amount createInstance(final BigDecimal value) {
			return new Amount(value);
		}
	}

	@Test
	public void testEqual1() {
		final Amount amount1 = new Amount(new BigDecimal("1234.56"));
		final Amount amount2 = new Amount(1234.56);
		final Amount amount3 = new Amount("1234.56");
		assertTrue(amount1.equals(amount2));
		assertTrue(amount1.equals(amount3));
	}

	@Test
	public void testEqual2() {
		final Amount amount1 = new Amount(new BigDecimal("1234.5"));
		final Amount amount2 = new Amount(1234.5);
		final Amount amount3 = new Amount("1234.5");
		assertTrue(amount1.equals(amount2));
		assertTrue(amount1.equals(amount3));
	}

	@Test
	public void testEqual3() {
		final Amount amount1 = new Amount(1234);
		final Amount amount2 = new Amount(1234.0);
		final Amount amount3 = new Amount(1234.00);
		assertTrue(amount1.equals(amount2));
		assertTrue(amount1.equals(amount3));
	}

	@Test
	public void testRounding1() {
		final Amount amount1 = new Amount(1234.554);
		final Amount amount2 = new Amount(1234.555);
		final Amount amount3 = new Amount(1234.556);

		final Amount amountDown = new Amount(1234.55);
		final Amount amountUp = new Amount(1234.56);

		assertTrue(amount1.equals(amountDown));
		assertTrue(amount2.equals(amountUp));
		assertTrue(amount3.equals(amountUp));
	}

	@Test
	public void testAddition1() {
		final Amount amount1 = new Amount(100.10);
		final Amount amount2 = new Amount(234.25);
		final Amount amount3 = new Amount(334.35);

		assertTrue(amount3.equals(amount1.add(amount2)));
	}

	@Test
	public void testAddition2() {
		final Amount amount1 = new Amount(100);
		final Amount amount2 = new Amount(134);
		final Amount amount3 = new Amount(234.00);

		assertTrue(amount3.equals(amount1.add(amount2)));
	}

	@Test
	public void testSubtraction1() {
		final Amount amount1 = new Amount(234.25);
		final Amount amount2 = new Amount(100.10);
		final Amount amount3 = new Amount(134.15);

		assertTrue(amount3.equals(amount1.subtract(amount2)));
	}

	@Test
	public void testSubtraction2() {
		final Amount amount1 = new Amount(234);
		final Amount amount2 = new Amount(100);
		final Amount amount3 = new Amount(134.00);

		assertTrue(amount3.equals(amount1.subtract(amount2)));
	}

	@Test
	public void testPrecision1() {
		final Amount amount1 = new Amount(234.123);
		final Amount amount2 = new Amount(234.12);
		assertTrue(amount1.equals(amount2));
	}
}
