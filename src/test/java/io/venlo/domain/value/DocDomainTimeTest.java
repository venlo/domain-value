/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainTime;
import io.venlo.domain.value.meta.TimeMeta;
import io.venlo.domain.value.meta.TimePrecisionMeta;

public class DocDomainTimeTest {

	class StartTime extends DomainTime<StartTime> {

		public StartTime(final LocalTime value) {

			super(TimeMeta.create(TimePrecisionMeta.MINUTES), value);
		}

		@Override
		protected StartTime createInstance(final LocalTime value) {
			return new StartTime(value);
		}
	}

	@Test
	public void test() {

		final StartTime d1 = new StartTime(LocalTime.now());
		final StartTime d2 = d1.plusMinutes(5);
		final StartTime d3 = d1.minusHours(5);
		final StartTime d4 = d1.plusSeconds(10);
		final StartTime d5 = d1.minusSeconds(20);

		System.out.println("d1                 : " + d1);
		System.out.println("d1.plusMinutes(5)  : " + d2);
		System.out.println("d1.minusHours(5)   : " + d3);
		System.out.println("d1.plusSeconds(20) : " + d4);
		System.out.println("d1.minusSeconds(20): " + d5);

		System.out.println("d2.isBefore(d1)    : " + d2.isBefore(d1));
		System.out.println("d2.isAfter(d1)     : " + d2.isAfter(d1));

		System.out.println("d1.equals(d4)      : " + d1.equals(d4));
		System.out.println("d1.equals(d5)      : " + d1.equals(d5));
	}
}