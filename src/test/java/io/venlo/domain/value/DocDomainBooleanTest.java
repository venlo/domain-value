/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainBoolean;

public class DocDomainBooleanTest {

	class Confirmed extends DomainBoolean<Confirmed> {
		
		public Confirmed(final Boolean value) {
			super(value);
		}

		@Override
		protected Confirmed createInstance(final Boolean value) {
			return new Confirmed(value);
		}
	}
	
	@Test
	public void test() {
		
		final Confirmed confirmed = new Confirmed(false);
		
		System.out.println("negate   : " + confirmed.negate());
		System.out.println("is-false : " + confirmed.isFalse());
		System.out.println("is-true  : " + confirmed.isTrue());
	}
}