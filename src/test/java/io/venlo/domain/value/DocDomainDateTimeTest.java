/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainDateTime;
import io.venlo.domain.value.meta.DateTimeMeta;
import io.venlo.domain.value.meta.TimePrecisionMeta;

public class DocDomainDateTimeTest {

	class TimeStamp extends DomainDateTime<TimeStamp> {

		public TimeStamp(final LocalDateTime value) {

			super(DateTimeMeta.create(TimePrecisionMeta.MINUTES), value);
		}

		@Override
		protected TimeStamp createInstance(final LocalDateTime value) {
			return new TimeStamp(value);
		}
	}

	@Test
	public void test() {

		final TimeStamp d1 = new TimeStamp(LocalDateTime.now());
		final TimeStamp d2 = d1.plusMinutes(5);
		final TimeStamp d3 = d1.minusHours(5);
		final TimeStamp d4 = d1.plusSeconds(10);
		final TimeStamp d5 = d1.minusSeconds(20);

		System.out.println("d1                 : " + d1);
		System.out.println("d1.plusMinutes(5)  : " + d2);
		System.out.println("d1.minusHours(5)   : " + d3);
		System.out.println("d1.plusSeconds(20) : " + d4);
		System.out.println("d1.minusSeconds(20): " + d5);

		System.out.println("d2.isBefore(d1)    : " + d2.isBefore(d1));
		System.out.println("d2.isAfter(d1)     : " + d2.isAfter(d1));

		System.out.println("d1.equals(d4)      : " + d1.equals(d4));
		System.out.println("d1.equals(d5)      : " + d1.equals(d5));
	}
}