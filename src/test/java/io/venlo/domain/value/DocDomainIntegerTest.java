/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import io.venlo.domain.value.DomainInteger;
import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.IntegerMeta;

public class DocDomainIntegerTest {

	class PortNumber extends DomainInteger<PortNumber> {

		public PortNumber(final Integer value) {
			super(IntegerMeta.create(0, 65535), value);
		}

		@Override
		protected PortNumber createInstance(final Integer value) {
			return new PortNumber(value);
		}
	}

	@Test
	public void test1() {

		final PortNumber d1 = new PortNumber(80);
		final PortNumber d2 = new PortNumber(8000);
		final PortNumber d3 = d1.add(d2);

		System.out.println("d1 : " + d1);
		System.out.println("d2 : " + d2);
		System.out.println("d3 : " + d3);
		System.out.println("d1.isNegative() : " + d1.isNegative());
		System.out.println("d1.isZero()     : " + d1.isZero());
	}

	@Test
	public void test2() {

		assertThrows(DomainValueException.class, () -> {
			new PortNumber(-1);
		});
	}
}