/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.math.BigDecimal;

import io.venlo.domain.value.base.BaseDecimal;
import io.venlo.domain.value.meta.DecimalMeta;

public abstract class DomainDecimal<T> extends BaseDecimal {

	private static final long serialVersionUID = 1L;

	protected DomainDecimal(final DecimalMeta meta, final BigDecimal value) {
		super(meta, value);
	}

	protected DomainDecimal(final DecimalMeta meta, final double doubleValue) {
		super(meta, doubleValue);
	}

	protected DomainDecimal(final DecimalMeta meta, final String stringValue) {
		super(meta, stringValue);
	}

	protected abstract T createInstance(final BigDecimal value);

	public T negate() {
		return createInstance(getValue().negate());
	}

	public T absolute() {
		return createInstance(getValue().abs());
	}

	public T add(final BigDecimal augend) {
		return createInstance(getValue().add(augend));
	}

	public T add(final BaseDecimal augend) {
		return add(augend.getValue());
	}

	public T subtract(final BigDecimal subtrahend) {
		return createInstance(getValue().subtract(subtrahend));
	}

	public T subtract(final BaseDecimal subtrahend) {
		return subtract(subtrahend.getValue());
	}

	public T multiply(final BigDecimal multiplicand) {
		return createInstance(
				getValue().multiply(multiplicand, 
				getMeta().getMathContext()));
	}

	public T multiply(final BaseDecimal multiplicand) {
		return multiply(multiplicand.getValue());
	}

	public T divide(final BigDecimal divisor) {
		return createInstance(getValue().divide(divisor, getMeta().getMathContext()));
	}

	public T divide(final BaseDecimal divisor) {
		return divide(divisor.getValue());
	}

	public T percentage(final BaseDecimal percentage) {
		if (percentage.isNegative()) {
			throw DomainValueException.create("Negative percentage not allowed.");
		}
		
		return createInstance(
				getValue().multiply(percentage.getValue(), 
				getMeta().getMathContext()).divide(DECIMAL_HUNDRED, 
				getMeta().getMathContext()));
	}

	public T max(final BaseDecimal operand) {
		if (getValue().compareTo(operand.getValue()) >= 0) {
			return (T) this;
		}
		return createInstance(operand.getValue());
	}

	public T min(final BaseDecimal operand) {
		if (getValue().compareTo(operand.getValue()) <= 0) {
			return (T) this;
		}
		return createInstance(operand.getValue());
	}
}
