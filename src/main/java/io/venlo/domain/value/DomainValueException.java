/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import org.eclipse.jdt.annotation.Nullable;

public class DomainValueException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private DomainValueException(final String message, @Nullable final Exception e) {
		super(message, e);
	}

	public static DomainValueException create(final String message, final Exception e) {
		return new DomainValueException(message, e);
	}

	public static DomainValueException create(final String message) {
		return new DomainValueException(message, null);
	}
}
