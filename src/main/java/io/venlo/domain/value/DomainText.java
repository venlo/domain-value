/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import io.venlo.domain.value.base.BaseString;
import io.venlo.domain.value.base.BaseText;
import io.venlo.domain.value.meta.TextMeta;

public abstract class DomainText<T> extends BaseText {

	protected DomainText(final TextMeta meta, final String value) {
		super(meta, value);
	}

	protected abstract T createInstance(final String value);

	public T append(final BaseString stringValue) {
		return createInstance(getValue() + stringValue);
	}

	public T append(final String stringValue) {
		return createInstance(getValue() + stringValue);
	}
}
