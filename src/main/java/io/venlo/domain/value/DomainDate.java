/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.time.LocalDate;

import io.venlo.domain.value.base.BaseDate;

public abstract class DomainDate<T> extends BaseDate {

	protected DomainDate(final LocalDate value) {
		super(value);
	}

	protected abstract T createInstance(final LocalDate value);

	public T plusDays(final int days) {
		return createInstance(getValue().plusDays(days));
	}

	public T minusDays(final int days) {
		return createInstance(getValue().minusDays(days));
	}
}
