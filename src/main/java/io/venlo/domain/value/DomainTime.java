/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.time.LocalTime;

import io.venlo.domain.value.base.BaseTime;
import io.venlo.domain.value.meta.TimeMeta;

public abstract class DomainTime<T> extends BaseTime {

	protected DomainTime(final TimeMeta meta, final LocalTime value) {
		super(meta, value);
	}

	protected abstract T createInstance(final LocalTime value);

	public T plusSeconds(final int seconds) {
		return createInstance(getValue().plusSeconds(seconds));
	}

	public T minusSeconds(final int seconds) {
		return createInstance(getValue().minusSeconds(seconds));
	}

	public T plusMinutes(final int seconds) {
		return createInstance(getValue().plusMinutes(seconds));
	}

	public T minusMinutes(final int seconds) {
		return createInstance(getValue().minusMinutes(seconds));
	}

	public T plusHours(final int hours) {
		return createInstance(getValue().plusHours(hours));
	}

	public T minusHours(final int hours) {
		return createInstance(getValue().minusHours(hours));
	}
}
