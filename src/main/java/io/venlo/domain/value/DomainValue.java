/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.io.Serializable;

public interface DomainValue<P> extends Serializable {

	P getValue();
}
