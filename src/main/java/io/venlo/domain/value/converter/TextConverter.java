/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.TextMeta;

public class TextConverter implements ValueConverter<String> {

	private final TextMeta textMeta;

	private TextConverter(final TextMeta textMeta) {
		super();
		this.textMeta = textMeta;
	}

	public static TextConverter create(final TextMeta textMeta) {

		return new TextConverter(textMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		
		if (value == null || (value instanceof String)) {
			return toStringTyped((String) value);
		}
		throw DomainValueException.create("Invalid String value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final String stringValue) {

		if (stringValue != null) {
			return stringValue.trim();
		}
		return "";
	}

	@Override
	@Nullable
	public String fromString(final String stringValue) {

		// Remove trailing tabs
		String trimmedValue = stringValue.trim();
		while (trimmedValue.endsWith("\t")) {
			trimmedValue = trimmedValue.substring(0, trimmedValue.length() - 1);
		}
		if (!trimmedValue.isBlank()) {
			return trimmedValue;
		}

		return "";
	}
}
