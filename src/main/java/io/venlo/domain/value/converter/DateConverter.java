/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.DateMeta;

public class DateConverter implements ValueConverter<LocalDate> {

	private final DateMeta dateMeta;
	private final DateTimeFormatter dateFormatter;

	private DateConverter(final DateMeta dateMeta) {
		this.dateMeta = dateMeta;
		dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	}

	public static DateConverter create(final DateMeta dateMeta) {
		return new DateConverter(dateMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof LocalDate)) {
			return toStringTyped((LocalDate) value);
		}
		throw DomainValueException.create("Invalid LocalDate value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final LocalDate date) {

		if (date == null) {
			return "";
		}

		return date.format(dateFormatter);
	}

	@Override
	@Nullable
	public LocalDate fromString(final String stringValue) {
		return DateTimeConverterUtil.toLocalDate(stringValue);
	}
}
