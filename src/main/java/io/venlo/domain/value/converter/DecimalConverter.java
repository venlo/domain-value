/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.DecimalMeta;

public class DecimalConverter implements ValueConverter<BigDecimal> {

	private final DecimalFormat formatter;
	private final DecimalMeta decimalMeta;

	private DecimalConverter(final DecimalMeta decimalMeta) {
		super();
		this.decimalMeta = decimalMeta;

		/*
		 * e.g. precision=8, scale = 2: ###,###.##
		 */
		String format = "";

		for (int i = 1; i <= decimalMeta.getScale(); i++) {
			format = "0" + format;
		}

		if (decimalMeta.getScale() > 0) {
			format = "." + format;
		}

		for (int i = 1; i <= decimalMeta.getPrecision() - decimalMeta.getScale(); i++) {

			if (i == 4 || i == 7 || i == 10) {
				format = "," + format;
			}
			if (i == 1) {
				format = "0" + format;
			} else {
				format = "#" + format;
			}
		}

		final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');

		formatter = new DecimalFormat(format);
		formatter.setDecimalFormatSymbols(symbols);
	}

	public static DecimalConverter create(final DecimalMeta decimalMeta) {
		return new DecimalConverter(decimalMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof BigDecimal)) {
			return toStringTyped((BigDecimal) value);
		}
		throw DomainValueException.create("Invalid BigDecimal value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final BigDecimal value) {
		if (value == null) {
			return "";
		}
		return formatter.format(value);
	}

	@Override
	@Nullable
	public BigDecimal fromString(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}

		final ParsePosition pos = new ParsePosition(0);
		final Number decimal = formatter.parse(stringValue, pos);

		if (decimal == null) {
			throw DomainValueException.create("Invalid Decimal value: " + stringValue);
		}

		final BigDecimal bigDecimalValue = BigDecimal.valueOf(decimal.doubleValue()).setScale(decimalMeta.getScale(),
				decimalMeta.getRoundingMode());

		return bigDecimalValue;
	}

}
