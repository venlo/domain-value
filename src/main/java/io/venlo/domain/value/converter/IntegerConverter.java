/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.text.NumberFormat;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.IntegerMeta;

public class IntegerConverter implements ValueConverter<Integer> {

	private final IntegerMeta integerMeta;

	private IntegerConverter(final IntegerMeta integerMeta) {
		super();
		this.integerMeta = integerMeta;
	}

	public static IntegerConverter create(final IntegerMeta integerMeta) {
		return new IntegerConverter(integerMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		
		if (value == null || (value instanceof Integer)) {
			return toStringTyped((Integer) value);
		}
		throw DomainValueException.create("Invalid Integer value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final Integer value) {
		if (value == null) {
			return "";
		}
		return value.toString();
	}

	@Override
	@Nullable
	public Integer fromString(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}

		try {
			final Number number = NumberFormat.getNumberInstance(java.util.Locale.US).parse(stringValue);
			return number.intValue();

		} catch (final Exception e) {
			throw DomainValueException.create("Invalid Integer value: " + stringValue, e);
		}
	}
}
