/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.LongMeta;

public class LongConverter implements ValueConverter<Long> {

	private final LongMeta longMeta;

	private LongConverter(final LongMeta longMeta) {
		this.longMeta = longMeta;
	}

	public static LongConverter create(final LongMeta longMeta) {
		return new LongConverter(longMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		
		if (value == null || (value instanceof Long)) {
			return toStringTyped((Long) value);
		}
		throw DomainValueException.create("Invalid Long value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final Long value) {
		if (value == null) {
			return "";
		}
		return value.toString();
	}

	@Override
	@Nullable
	public Long fromString(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}
		try {
			return Long.parseLong(stringValue);

		} catch (final Exception e) {
			throw DomainValueException.create("Invalid Long value: " + stringValue, e);
		}
	}

}
