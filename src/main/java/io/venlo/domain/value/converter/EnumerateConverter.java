/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.EnumerateItem;
import io.venlo.domain.value.meta.EnumerateMeta;

public class EnumerateConverter implements ValueConverter<EnumerateItem> {

	final EnumerateMeta enumerateMeta;

	private EnumerateConverter(final EnumerateMeta enumerateMeta) {
		super();
		this.enumerateMeta = enumerateMeta;
	}

	public static EnumerateConverter create(final EnumerateMeta enumerateMeta) {
		return new EnumerateConverter(enumerateMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof EnumerateItem)) {
			return toStringTyped((EnumerateItem) value);
		}
		throw DomainValueException.create("Invalid EnumerateItem value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final EnumerateItem enumerate) {
		if (enumerate == null) {
			return "";
		}
		return enumerate.toString();
	}

	@Override
	@Nullable
	public EnumerateItem fromString(final String code) {

		if (code.trim().isEmpty()) {
			return null;
		}

		return enumerateMeta.valueOfCode(code);
	}

}
