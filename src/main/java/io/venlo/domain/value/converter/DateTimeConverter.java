/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.DateTimeMeta;

public class DateTimeConverter implements ValueConverter<LocalDateTime> {

	private DateTimeFormatter dateFormatter;
	private final DateTimeMeta dateTimeMeta;
	private final ZoneId timeZone;

	private DateTimeConverter(final DateTimeMeta dateTimeMeta, final ZoneId timeZone) {
		super();
		this.dateTimeMeta = dateTimeMeta;
		this.timeZone = timeZone;

		switch (dateTimeMeta.getPrecision()) {
		case MILLISECONDS:
			dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.nn");
			break;

		case SECONDS:
		default:
			dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
			break;

		case MINUTES:
			dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			break;
		}
		dateFormatter = dateFormatter.withZone(timeZone);
	}

	public static DateTimeConverter create(final DateTimeMeta dateTimeMeta) {
		return new DateTimeConverter(dateTimeMeta, ZoneId.systemDefault());
	}

	public static DateTimeConverter create(final DateTimeMeta dateTimeMeta, final ZoneId timeZone) {
		return new DateTimeConverter(dateTimeMeta, timeZone);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof LocalDateTime)) {
			return toStringTyped((LocalDateTime) value);
		}
		throw DomainValueException.create("Invalid LocalDateTime value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final LocalDateTime dateTime) {

		if (dateTime == null) {
			return "";
		}

		final String value = dateTime.format(dateFormatter);

		return value;
	}

	@Override
	@Nullable
	public LocalDateTime fromString(final String stringValue) {

		@Nullable
		final LocalDateTime value = DateTimeConverterUtil.toLocalDateTime(stringValue);

		return value;
	}

}
