/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.StringMeta;

public class StringConverter implements ValueConverter<String> {

	private final StringMeta stringMeta;

	private StringConverter(final StringMeta stringMeta) {
		super();
		this.stringMeta = stringMeta;
	}

	public static StringConverter create(final StringMeta stringMeta) {
		return new StringConverter(stringMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof String)) {
			return toStringTyped((String) value);
		}
		throw DomainValueException.create("Invalid String value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final String stringValue) {
		if (stringValue != null) {
			return stringValue.trim();
		}
		return "";
	}

	@Override
	@Nullable
	public String fromString(final String stringValue) {
		return stringValue.trim();
	}

}
