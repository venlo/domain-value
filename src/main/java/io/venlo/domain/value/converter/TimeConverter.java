/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.TimeMeta;

public class TimeConverter implements ValueConverter<LocalTime> {

	private final TimeMeta timeMeta;
	private final DateTimeFormatter timeFormatter;

	private TimeConverter(final TimeMeta timeMeta) {
		this.timeMeta = timeMeta;
		timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
	}

	public static TimeConverter create(final TimeMeta timeMeta) {

		return new TimeConverter(timeMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof LocalTime)) {
			return toStringTyped((LocalTime) value);
		}
		throw DomainValueException.create("Invalid LocalTime value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final LocalTime date) {

		if (date == null) {
			return "";
		}

		return date.format(timeFormatter);
	}

	@Override
	@Nullable
	public LocalTime fromString(final String stringValue) {
		return DateTimeConverterUtil.toLocalTime(stringValue);
	}
}
