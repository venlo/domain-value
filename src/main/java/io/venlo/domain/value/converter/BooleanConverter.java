/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.meta.BooleanMeta;

public class BooleanConverter implements ValueConverter<Boolean> {

	public static final String TRUE = "T";
	public static final String FALSE = "F";

	public static final String TRUE_LONG = "TRUE";
	public static final String FALSE_LONG = "FALSE";

	private final BooleanMeta booleanMeta;

	private BooleanConverter(final BooleanMeta booleanMeta) {
		this.booleanMeta = booleanMeta;
	}

	public static BooleanConverter create(final BooleanMeta booleanMeta) {

		return new BooleanConverter(booleanMeta);
	}

	@Override
	public String toString(@Nullable final Object value) {
		if (value == null || (value instanceof Boolean)) {
			return toStringTyped((Boolean) value);
		}
		throw DomainValueException.create("Invalid Boolean value: " + value);
	}

	@Override
	public String toStringTyped(@Nullable final Boolean value) {
		if (value == null) {
			return "";
		}
		if (value) {
			return TRUE;
		}
		return FALSE;
	}

	@Override
	@Nullable
	public Boolean fromString(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}

		final String upperStringValue = stringValue.toUpperCase();

		if (TRUE.equals(upperStringValue)) {
			return true;
		}
		if (FALSE.equals(upperStringValue)) {
			return false;
		}

		if (TRUE_LONG.equals(upperStringValue)) {
			return true;
		}
		if (FALSE_LONG.equals(upperStringValue)) {
			return false;
		}

		throw DomainValueException.create("Invalid Boolean value: " + upperStringValue);
	}
}
