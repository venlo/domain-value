/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;

class DateTimeConverterUtil {

	@Nullable
	static LocalTime toLocalTime(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}

		@Nullable
		Integer hourOfDay = null;
		@Nullable
		Integer minuteOfHour = null;

		final String timeString = stringValue.trim();
		if (timeString.contains(":")) {
			// e.g. 10:00
			final String[] parts = timeString.split(":");
			if (parts.length >= 1) {
				hourOfDay = Integer.parseInt(parts[0]);
			}
			if (parts.length >= 2) {
				minuteOfHour = Integer.parseInt(parts[1]);
			}
		} else {
			// e.g. 9, 1000
			if (timeString.length() == 1) {
				// e.g. 9
				hourOfDay = Integer.parseInt(timeString.substring(0, 1));
			}
			if (timeString.length() == 2) {
				// e.g. 10
				hourOfDay = Integer.parseInt(timeString.substring(0, 2));
			}
			if (timeString.length() == 3) {
				// e.g. 930
				hourOfDay = Integer.parseInt(timeString.substring(0, 1));
				minuteOfHour = Integer.parseInt(timeString.substring(1, 3));
			}
			if (timeString.length() == 4) {
				// e.g. 1030
				hourOfDay = Integer.parseInt(timeString.substring(0, 2));
				minuteOfHour = Integer.parseInt(timeString.substring(2, 4));
			}
		}
		if (minuteOfHour == null) {
			minuteOfHour = 0;
		}

		if (hourOfDay == null) {
			throw DomainValueException.create("Invalid time: " + stringValue);
		}

		return LocalTime.of(hourOfDay, minuteOfHour);
	}

	@Nullable
	static LocalDateTime toLocalDateTime(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}

		final String dateTimeString = stringValue.trim();

		String dateString = null;
		String timeString = null;

		final String[] parts = dateTimeString.split(" ");
		if (parts.length == 0) {
			return null;
		}
		if (parts.length >= 1) { 
			dateString = parts[0];
		}
		if (parts.length >= 2) {
			timeString = parts[1];
		}
		LocalDate localDate = null;
		LocalTime localTime = null;
		if (dateString != null) {
			localDate = toLocalDate(dateString);
		}
		if (timeString != null) {
			localTime = toLocalTime(timeString);
		}
		if (localTime == null) {
			localTime = LocalTime.of(8, 0);
		}

		if (localDate == null || localTime == null) {
			throw DomainValueException.create("Invalid date time: " + stringValue);
		}

		return LocalDateTime.of(localDate, localTime);
	}

	@Nullable
	public static LocalDate toLocalDate(final String stringValue) {

		if (stringValue.trim().isEmpty()) {
			return null;
		}

		String dayString = null;
		String monthString = null;
		String yearString = null;
		
		Integer year = null;
		Integer monthOfYear = null;
		Integer dayOfMonth = null;

		final LocalDate currentDate = LocalDate.now();
		String dateString = stringValue.trim();
		dateString = dateString.replace("/", "-");
		dateString = dateString.replace("\\", "-");

		if (dateString.contains("-")) {
			// 10-10-2000
			// 1-10-11
			final String[] parts = dateString.split("-");

			if (parts.length >= 1) {
				dayString = parts[0];
			}
			if (parts.length >= 2) {
				monthString = parts[1];
			}
			if (parts.length >= 3) {
				yearString = parts[2];
			}
		} else {
			// 10102000
			// 011011
			if (dateString.length() == 1) {
				dayString = dateString.substring(0, 1);
			}
			if (dateString.length() >= 2) {
				dayString = dateString.substring(0, 2);
			}
			if (dateString.length() >= 4) {
				monthString = dateString.substring(2, 4);
			}
			if (dateString.length() >= 6) {
				yearString = dateString.substring(4);
			}
		}
		// Day
		if (dayString != null) {
			dayOfMonth = Integer.parseInt(dayString);
		}
		// Month
		if (monthString != null) {
			monthOfYear = Integer.parseInt(monthString);
		} else {
			monthOfYear = currentDate.getMonth().getValue();
		}
		// Year
		if (yearString != null) {
			year = Integer.parseInt(yearString);
			if (year < 100) {
				year = year + 2000;
			}
		} else {
			year = currentDate.getYear();
		}

		if (dayOfMonth == null) {
			throw DomainValueException.create("Invalid date: " + dateString);
		}

		return LocalDate.of(year, monthOfYear, dayOfMonth);
	}

}
