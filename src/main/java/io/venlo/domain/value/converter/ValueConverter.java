/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.converter;

import java.io.Serializable;

import org.eclipse.jdt.annotation.Nullable;

public interface ValueConverter<T> extends Serializable {

	String toString(@Nullable Object value);

	String toStringTyped(@Nullable T value);

	@Nullable
	T fromString(String stringValue);
}
