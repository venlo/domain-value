/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import io.venlo.domain.value.base.BaseInteger;
import io.venlo.domain.value.meta.IntegerMeta;

public abstract class DomainInteger<T> extends BaseInteger {

	protected DomainInteger(final IntegerMeta meta, final Integer value) {
		super(meta, value);
	}

	protected abstract T createInstance(final Integer value);

	public T absolute() {
		
		if (this.isNegative()) {
			return this.negate();
		}
		return (T) this;
	}

	public T add(final BaseInteger augend) {
		return createInstance(getValue() + augend.getValue());
	}

	public T subtract(final BaseInteger subtrahend) {
		return createInstance(getValue() - subtrahend.getValue());
	}

	public T negate() {
		return createInstance(getValue() * -1);
	}

	public T multiply(final BaseInteger multiplicand) {
		return createInstance(getValue() * multiplicand.getValue());
	}

	public T multiply(final Integer multiplicand) {
		return createInstance(getValue() * multiplicand);
	}

	public T divide(final Integer divisor) {
		return createInstance(getValue() / divisor);
	}

	public T divide(final BaseInteger divisor) {
		return createInstance(getValue() / divisor.getValue());
	}

	public T max(final BaseInteger operand) {
		if (getValue().compareTo(operand.getValue()) >= 0) {
			return (T) this;
		}
		return createInstance(operand.getValue());
	}

	public T min(final BaseInteger operand) {
		if (getValue().compareTo(operand.getValue()) <= 0) {
			return (T) this;
		}
		return createInstance(operand.getValue());
	}

}
