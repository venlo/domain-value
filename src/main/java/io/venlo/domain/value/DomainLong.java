/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import io.venlo.domain.value.base.BaseLong;
import io.venlo.domain.value.meta.LongMeta;

public abstract class DomainLong<T> extends BaseLong {

	protected DomainLong(final LongMeta meta, final Long value) {
		super(meta, value);
	}

	protected abstract T createInstance(final Long value);

	public T absolute() {

		if (this.isNegative()) {
			return this.negate();
		}
		return (T) this;
	}

	public T add(final BaseLong augend) {
		return createInstance(getValue() + augend.getValue());
	}

	public T subtract(final BaseLong subtrahend) {
		return createInstance(getValue() - subtrahend.getValue());
	}

	public T negate() {
		return createInstance(getValue() * -1);
	}

	public T multiply(final BaseLong multiplicand) {
		return createInstance(getValue() * multiplicand.getValue());
	}

	public T multiply(final Long multiplicand) {
		return createInstance(getValue() * multiplicand);
	}

	public T divide(final Long divisor) {
		return createInstance(getValue() / divisor);
	}
	
	public T divide(final BaseLong divisor) {
		return createInstance(getValue() / divisor.getValue());
	}

	public T max(final BaseLong operand) {
		if (getValue().compareTo(operand.getValue()) >= 0) {
			return (T) this;
		}
		return createInstance(operand.getValue());
	}

	public T min(final BaseLong operand) {
		if (getValue().compareTo(operand.getValue()) <= 0) {
			return (T) this;
		}
		return createInstance(operand.getValue());
	}
}
