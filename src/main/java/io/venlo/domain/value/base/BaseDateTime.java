/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.DateTimeMeta;

public abstract class BaseDateTime extends AbstractDomainValue<LocalDateTime> implements Comparable<BaseDateTime> {

	private static final TemporalField weekFields = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();

	private final DateTimeMeta meta;
	private final LocalDateTime value;

	protected BaseDateTime(final DateTimeMeta meta, final LocalDateTime value) {
		super();
		this.meta = meta;
		this.value = truncateToPrecision(meta, value);

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}
	}

	@Override
	public LocalDateTime getValue() {
		return value;
	}

	public DateTimeMeta getMeta() {
		return meta;
	}

	public boolean isAfter(final LocalDateTime date) {
		return getValue().isAfter(date);
	}

	public boolean isAfter(final BaseDateTime date) {
		return getValue().isAfter(date.getValue());
	}

	public boolean isBefore(final LocalDateTime date) {
		return getValue().isBefore(date);
	}

	public boolean isBefore(final BaseDateTime date) {
		return getValue().isBefore(date.getValue());
	}

	public boolean isAfterNow() {
		return getValue().isAfter(LocalDateTime.now());
	}

	public boolean isBeforeNow() {
		return getValue().isBefore(LocalDateTime.now());
	}

	public int getDayOfMonth() {
		return getValue().getDayOfMonth();
	}

	public DayOfWeek getDayOfWeek() {
		return getValue().getDayOfWeek();
	}

	public int getDayOfYear() {
		return getValue().getDayOfYear();
	}

	public int getHour() {
		return getValue().getHour();
	}

	public int getMinute() {
		return getValue().getMinute();
	}

	public int getWeek() {
		return getValue().get(weekFields);
	}

	public Month getMonth() {
		return getValue().getMonth();
	}

	public int getSecond() {
		return getValue().getSecond();
	}

	public int getYear() {
		return getValue().getYear();
	}

	public LocalDate toLocalDate() {
		return getValue().toLocalDate();
	}

	public LocalTime toLocalTime() {
		return getValue().toLocalTime();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseDateTime other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}

	private static LocalDateTime truncateToPrecision(final DateTimeMeta meta, final LocalDateTime value) {

		switch (meta.getPrecision()) {
		case MILLISECONDS:
			// Treat as Nanoseconds
			return value;

		case SECONDS:
			if (value.getNano() != 0) {
				return value.withNano(0);
			} else {
				return value;
			}

		case MINUTES:
			if (value.getSecond() != 0 || value.getNano() != 0) {
				return value.withSecond(0).withNano(0);
			} else {
				return value;
			}

		default:
			return value;
		}
	}
}
