/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.LongMeta;

public abstract class BaseLong extends AbstractDomainValue<Long> implements Comparable<BaseLong> {

	private final LongMeta meta;
	private final Long value;

	protected BaseLong(final LongMeta meta, final Long value) {
		super();

		this.meta = meta;
		this.value = value;

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}

		if (value != 0) {
			if (value < meta.getMinValue()) {
				throw DomainValueException.create("Value exceeds minimum value " + meta.getMinValue() + ".");
			}

			if (value > meta.getMaxValue()) {
				throw DomainValueException.create("Value exceeds maximum value " + meta.getMaxValue() + ".");
			}
		}
	}

	@Override
	public Long getValue() {
		return value;
	}

	public LongMeta getMeta() {
		return meta;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public boolean isZero() {
		return getValue().compareTo(Long.valueOf(0)) == 0;
	}

	public boolean isOne() {
		return getValue().compareTo(Long.valueOf(1)) == 0;
	}

	public boolean isNegativeOne() {
		return getValue().compareTo(Long.valueOf(-1)) == 0;
	}

	public boolean isNegative() {
		return getValue().compareTo(Long.valueOf(0)) == -1;
	}

	public boolean isPositive() {
		return !isNegative() && !isZero();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseLong other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}
}
