/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import java.time.Duration;
import java.time.LocalTime;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.TimeMeta;

public abstract class BaseTime extends AbstractDomainValue<LocalTime> implements Comparable<BaseTime> {

	private final TimeMeta meta;
	private final LocalTime value;

	protected BaseTime(final TimeMeta meta, final LocalTime value) {
		super();
		this.meta = meta;
		this.value = truncateToPrecision(meta, value);

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}
	}

	@Override
	public LocalTime getValue() {
		return value;
	}

	public TimeMeta getMeta() {
		return meta;
	}

	public boolean isAfter(@Nullable final BaseTime time) {
		if (time == null) {
			throw DomainValueException.create("Null value not allowed in time comparison.");
		}
		return getValue().isAfter(time.getValue());
	}

	public boolean isBefore(@Nullable final BaseTime time) {
		if (time == null) {
			throw DomainValueException.create("Null value not allowed in time comparison.");
		}
		return getValue().isBefore(time.getValue());
	}

	public Duration getDurationTo(@Nullable final BaseTime timeEndExclusive) {
		if (timeEndExclusive == null) {
			throw DomainValueException.create("Null value not allowed in time comparison.");
		}
		return Duration.between(getValue(), timeEndExclusive.getValue());
	}

	public int getHour() {
		return getValue().getHour();
	}

	public int getMinute() {
		return getValue().getMinute();
	}

	public int getNano() {
		return getValue().getNano();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseTime other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}

	private static LocalTime truncateToPrecision(final TimeMeta meta, final LocalTime value) {

		switch (meta.getPrecision()) {
		case MILLISECONDS:
			// Treat as Nanoseconds
			return value;

		case SECONDS:
			if (value.getNano() != 0) {
				return value.withNano(0);
			} else {
				return value;
			}

		case MINUTES:
			if (value.getSecond() != 0 || value.getNano() != 0) {
				return value.withSecond(0).withNano(0);
			} else {
				return value;
			}

		default:
			return value;
		}
	}
}
