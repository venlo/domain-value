/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base.internal;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValue;

public abstract class AbstractDomainValue<P> implements DomainValue<P> {

	protected AbstractDomainValue() {
		super();
	}

	@Override
	public String toString() {
		return String.valueOf(getValue());
	}

	@Override
	@NonNull
	public abstract P getValue();

	@Override
	public int hashCode() {
		return getValue().hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object otherObject) {

		if (this == otherObject) {
			return true;
		}
		
		if (otherObject == null) {
			return false;
		}

		final DomainValue<?> otherDomainValue = (DomainValue<?>) otherObject;

		return getValue().equals(otherDomainValue.getValue());
	}
}
