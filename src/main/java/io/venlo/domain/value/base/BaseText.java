/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.TextMeta;

public abstract class BaseText extends AbstractDomainValue<String> implements Comparable<BaseText> {

	private final TextMeta meta;
	private final String value;

	protected BaseText(final TextMeta meta, final String value) {
		super();
		this.meta = meta;
		this.value = value;

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}
	}

	@Override
	public String getValue() {
		return value;
	}

	public TextMeta getMeta() {
		return meta;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public boolean isEmpty() {
		return getValue().trim().isEmpty();
	}

	public boolean isNotEmpty() {
		return !isEmpty();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseText other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}

}
