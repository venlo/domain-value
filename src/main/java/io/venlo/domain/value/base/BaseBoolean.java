/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.BooleanMeta;

public abstract class BaseBoolean extends AbstractDomainValue<Boolean> implements Comparable<BaseBoolean> {

	private final BooleanMeta meta;
	private final Boolean value;

	private BaseBoolean(final BooleanMeta meta, final Boolean value) {
		super();
		this.meta = meta;
		this.value = value;

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}
	}

	protected BaseBoolean(final Boolean value) {
		this(BooleanMeta.create(), value);
	}

	@Override
	public Boolean getValue() {
		return value;
	}

	public BooleanMeta getMeta() {
		return meta;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public boolean isTrue() {
		return getValue() == true;
	}

	public boolean isFalse() {
		return getValue() == false;
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseBoolean other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}
}
