/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.StringMeta;

public abstract class BaseString extends AbstractDomainValue<String> implements Comparable<BaseString> {

	private final StringMeta meta;
	private final String value;

	protected BaseString(final StringMeta meta, final String value) {
		super();
		this.meta = meta;
		this.value = value;

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}

		if (value.length() > meta.getLength()) {
			throw DomainValueException.create("Value exceeds " + meta.getLength() + " characters.");
		}
	}

	@Override
	public String getValue() {
		return value;
	}

	public StringMeta getMeta() {
		return meta;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public String getLeft(final int length) {
		if (getValue().length() <= length) {
			return getValue();
		}
		return getValue().substring(0, length);
	}

	public boolean isEmpty() {
		return getValue().trim().isEmpty();
	}

	public boolean isNotEmpty() {
		return !isEmpty();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseString other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}

}
