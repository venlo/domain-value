/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import java.math.BigDecimal;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.DecimalMeta;

public abstract class BaseDecimal extends AbstractDomainValue<BigDecimal> implements Comparable<BaseDecimal> {

	protected static final BigDecimal DECIMAL_HUNDRED = BigDecimal.valueOf(100.0);

	private final DecimalMeta meta;
	private final BigDecimal value;

	protected BaseDecimal(final DecimalMeta meta, final BigDecimal value) {
		super();
		this.meta = meta;
		this.value = adjust(meta, value);

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}
	}

	protected BaseDecimal(final DecimalMeta meta, final String stringValue) {
		this(meta, new BigDecimal(stringValue));
	}

	protected BaseDecimal(final DecimalMeta meta, final double doubleValue) {
		this(meta, new BigDecimal(doubleValue));
	}

	@Override
	public BigDecimal getValue() {
		return value;
	}

	protected DecimalMeta getMeta() {
		return meta;
	}

	private static BigDecimal adjust(final DecimalMeta meta, final BigDecimal value) {

		BigDecimal adjustedValue = value;

		if (adjustedValue.precision() != meta.getPrecision()) {
			adjustedValue = adjustedValue.round(meta.getMathContext());
		}

		if (adjustedValue.scale() != meta.getScale()) {
			adjustedValue = adjustedValue.setScale(meta.getScale(), meta.getRoundingMode());
		}

		return adjustedValue;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public void validate() {

		if (getValue().scale() > meta.getScale()) {
			throw DomainValueException.create("Scale has maximum of " + meta.getScale() + " digits.");
		}

		if (getValue().precision() > meta.getPrecision()) {
			throw DomainValueException.create("Precision has maximum of " + meta.getPrecision() + " digits.");
		}
	}

	public boolean isZero() {
		return getValue().compareTo(BigDecimal.ZERO) == 0;
	}

	public boolean isNonZero() {
		return getValue().compareTo(BigDecimal.ZERO) != 0;
	}

	public boolean isOne() {
		return getValue().compareTo(BigDecimal.ONE) == 0;
	}

	public boolean isNegativeOne() {
		return getValue().compareTo(BigDecimal.ONE.negate()) == 0;
	}

	public boolean isNegative() {
		return getValue().compareTo(BigDecimal.ZERO) == -1;
	}

	public boolean isPositive() {
		return !isNegative() && !isZero();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseDecimal other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}
}
