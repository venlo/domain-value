/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.meta.IntegerMeta;

public abstract class BaseInteger extends AbstractDomainValue<Integer> implements Comparable<BaseInteger> {

	private final IntegerMeta meta;
	private final Integer value;

	protected BaseInteger(final IntegerMeta meta, final Integer value) {
		super();
		this.meta = meta;
		this.value = value;

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}

		if (value != 0) {

			if (value < meta.getMinValue()) {
				throw DomainValueException
					.create("Value " + value + " exceeds minimum value " + meta.getMinValue() + ".");
			}

			if (value > meta.getMaxValue()) {
				throw DomainValueException
					.create("Value " + value + " exceeds maximum value " + meta.getMaxValue() + ".");
			}
		}
	}

	@Override
	public Integer getValue() {
		return value;
	}

	public IntegerMeta getMeta() {
		return meta;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public boolean isZero() {
		return getValue().compareTo(Integer.valueOf(0)) == 0;
	}

	public boolean isOne() {
		return getValue().compareTo(Integer.valueOf(1)) == 0;
	}

	public boolean isNegativeOne() {
		return getValue().compareTo(Integer.valueOf(-1)) == 0;
	}

	public boolean isNegative() {
		return getValue().compareTo(Integer.valueOf(0)) == -1;
	}

	public boolean isPositive() {
		return !isNegative() && !isZero();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseInteger other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());
	}
}
