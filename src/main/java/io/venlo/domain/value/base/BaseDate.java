/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.base;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.base.internal.AbstractDomainValue;
import io.venlo.domain.value.converter.DateConverter;
import io.venlo.domain.value.meta.DateMeta;

public abstract class BaseDate extends AbstractDomainValue<LocalDate> implements Comparable<BaseDate> {

	private static final DateMeta meta = DateMeta.create();
	private static final DateConverter converter = meta.getConverter();

	private static final TemporalField weekFields = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();

	private final LocalDate value;

	private BaseDate(final DateMeta meta, final LocalDate value) {
		super();
		this.value = value;

		if (value == null) {
			throw DomainValueException.create("Attribute can not be instantiated with value 'null'.");
		}
	}

	protected BaseDate(final LocalDate value) {
		this(meta, value);
	}

	@Override
	public LocalDate getValue() {
		return value;
	}

	public DateMeta getMeta() {
		return meta;
	}

	@Override
	@NonNull
	public String toString() {
		return converter.toString(getValue());
	}

	public static DateMeta meta() {
		return meta;
	}

	public static DateConverter converter() {
		return converter;
	}

	/******************************
	 * Additional methods
	 ******************************/
	public boolean isAfter(final BaseDate dateValue) {
		return getValue().isAfter(dateValue.getValue());
	}

	public boolean isBefore(final BaseDate dateValue) {
		return getValue().isBefore(dateValue.getValue());
	}

	public int getDayOfMonth() {
		return getValue().getDayOfMonth();
	}

	public DayOfWeek getDayOfWeek() {
		return getValue().getDayOfWeek();
	}

	public int getDayOfYear() {
		return getValue().getDayOfYear();
	}

	public int getWeek() {
		return getValue().get(weekFields);
	}

	public Month getMonth() {
		return getValue().getMonth();
	}

	public int getYear() {
		return getValue().getYear();
	}

	public LocalDate toLocalDate() {
		return getValue();
	}

	public LocalDateTime atStartOfDay() {
		return getValue().atStartOfDay();
	}

	/******************************
	 * System methods
	 ******************************/
	@Override
	public int compareTo(@Nullable final BaseDate other) {
		if (other == null) {
			return 1;
		}
		return getValue().compareTo(other.getValue());

	}
}
