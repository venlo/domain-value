/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.BooleanConverter;

public class BooleanMeta implements ValueTypeMeta, Serializable {

	private final BooleanConverter converter;

	private BooleanMeta() {
		super();
		converter = BooleanConverter.create(this);
	}

	public static BooleanMeta create() {
		return new BooleanMeta();
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.BOOLEAN;
	}

	@Override
	public BooleanConverter getConverter() {
		return converter;
	}

}
