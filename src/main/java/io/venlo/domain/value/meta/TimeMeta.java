/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.TimeConverter;

public class TimeMeta implements ValueTypeMeta, Serializable {

	private final TimePrecisionMeta precision;
	private final TimeConverter converter;

	private TimeMeta(final TimePrecisionMeta precision) {
		super();
		this.precision = precision;
		converter = TimeConverter.create(this);
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.TIME;
	}

	public static TimeMeta create(final TimePrecisionMeta precision) {
		return new TimeMeta(precision);
	}

	public TimePrecisionMeta getPrecision() {
		return precision;
	}

	@Override
	public TimeConverter getConverter() {
		return converter;
	}

}
