/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.DateConverter;

public class DateMeta implements ValueTypeMeta, Serializable {

	private final DateConverter converter;

	private DateMeta() {
		super();
		converter = DateConverter.create(this);
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.DATE;
	}

	public static DateMeta create() {
		return new DateMeta();
	}

	@Override
	public DateConverter getConverter() {
		return converter;
	}
}
