/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

import io.venlo.domain.value.DomainValueException;

public enum TimePrecisionMeta implements Serializable {
	MILLISECONDS,
	SECONDS,
	MINUTES;

	public LocalTime convert(final LocalTime value) {
		
		LocalTime newValue = null;
		
		switch (this) {
		case MILLISECONDS:
			newValue = value;
			break;
		case SECONDS:
			newValue = value.withNano(0);
			break;
		case MINUTES:
			newValue = value.withSecond(0).withNano(0);
			break;
		default:
			throw DomainValueException.create("Invalid time precision: " + this);
		}
		
		return newValue;
	}


	public LocalDateTime convert(final LocalDateTime value) {
		LocalDateTime newValue = null;
		
		switch (this) {
		case MILLISECONDS:
			newValue = value;
			break;
		case SECONDS:
			newValue = value.withNano(0);
			break;
		case MINUTES:
			newValue = value.withSecond(0).withNano(0);
			break;
		default:
			throw DomainValueException.create("Invalid time precision: " + this);
		}

		return newValue;
	}
}
