/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.DateTimeConverter;

public class DateTimeMeta implements ValueTypeMeta, Serializable {

	private final TimePrecisionMeta precision;
	private final DateTimeConverter converter;

	private DateTimeMeta(final TimePrecisionMeta precision) {
		super();
		this.precision = precision;

		converter = DateTimeConverter.create(this);
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.DATETIME;
	}

	public static DateTimeMeta create(final TimePrecisionMeta precision) {
		return new DateTimeMeta(precision);
	}

	public TimePrecisionMeta getPrecision() {
		return precision;
	}

	@Override
	public DateTimeConverter getConverter() {
		return converter;
	}
}
