/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.StringConverter;

public class StringMeta implements ValueTypeMeta, Serializable {

	private final int length;
	private final boolean uppercase;
	private final StringConverter converter;

	private StringMeta(final int length, final boolean uppercase) {
		super();
		this.length = length;
		this.uppercase = uppercase;

		converter = StringConverter.create(this);
	}

	public static StringMeta create(final int length, final boolean upperCase) {
		return new StringMeta(length, upperCase);
	}

	public int getLength() {
		return length;
	}

	public boolean isUpperCase() {
		return uppercase;
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.STRING;
	}

	public boolean isUppercase() {
		return uppercase;
	}

	@Override
	public StringConverter getConverter() {
		return converter;
	}
}
