/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import io.venlo.domain.value.converter.ValueConverter;

public interface ValueTypeMeta {

	PrimitiveType getPrimitiveType();

	<T> ValueConverter<T> getConverter();
}
