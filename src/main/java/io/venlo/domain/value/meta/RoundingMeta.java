/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;
import java.math.RoundingMode;

public enum RoundingMeta implements Serializable {

	CEILING(RoundingMode.CEILING),
	DOWN(RoundingMode.DOWN),
	FLOOR(RoundingMode.FLOOR),
	HALF_DOWN(RoundingMode.HALF_DOWN),
	HALF_UP(RoundingMode.HALF_UP),
	UP(RoundingMode.UP);

	private final RoundingMode roundingMode;

	private RoundingMeta(final RoundingMode roundingMode) {
		this.roundingMode = roundingMode;
	}

	public RoundingMode getRoundingMode() {
		return roundingMode;
	}
}
