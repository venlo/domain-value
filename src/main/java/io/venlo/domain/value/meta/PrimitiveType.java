/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

public enum PrimitiveType implements Serializable {
	BOOLEAN, 
	DATETIME, 
	DATE, 
	DECIMAL, 
	INTEGER,
//	LOCALE,
	LONG, 
	STRING, 
	TEXT, 
	TIME,
//	TIMEZONE
	;
}
