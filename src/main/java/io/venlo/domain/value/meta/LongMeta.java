/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.LongConverter;

public class LongMeta implements ValueTypeMeta, Serializable {

	private final long minValue;
	private final long maxValue;

	private final LongConverter converter;

	private LongMeta(final long minValue, final long maxValue) {
		super();

		this.minValue = minValue;
		this.maxValue = maxValue;

		converter = LongConverter.create(this);
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.LONG;
	}

	public long getMinValue() {
		return minValue;
	}

	public long getMaxValue() {
		return maxValue;
	}

	public static LongMeta create(final long minValue, final long maxValue) {
		return new LongMeta(minValue, maxValue);
	}

	@Override
	public LongConverter getConverter() {
		return converter;
	}
}
