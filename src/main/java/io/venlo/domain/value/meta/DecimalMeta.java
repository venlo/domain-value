/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;
import java.math.MathContext;
import java.math.RoundingMode;

import io.venlo.domain.value.converter.DecimalConverter;

public class DecimalMeta implements ValueTypeMeta, Serializable {

	private final int precision;
	private final int scale;
	private final RoundingMeta roundingMode;
	private final MathContext mathContext;
	private final DecimalConverter converter;

	private DecimalMeta(final int precision, final int scale, final RoundingMeta roundingMode) {
		super();
		this.precision = precision;
		this.scale = scale;
		this.roundingMode = roundingMode;

		this.mathContext = new MathContext(precision, roundingMode.getRoundingMode());

		converter = DecimalConverter.create(this);
	}

	public static DecimalMeta create(final int precision, final int scale, final RoundingMeta roundingMode) {
		return new DecimalMeta(precision, scale, roundingMode);
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.DECIMAL;
	}

	public int getPrecision() {
		return precision;
	}

	public int getScale() {
		return scale;
	}

	public RoundingMode getRoundingMode() {
		return roundingMode.getRoundingMode();
	}

	public MathContext getMathContext() {
		return mathContext;
	}

	@Override
	public DecimalConverter getConverter() {
		return converter;
	}
}
