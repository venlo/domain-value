/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.TextConverter;

public class TextMeta implements ValueTypeMeta, Serializable {

	private final TextConverter converter;

	private TextMeta() {
		super();

		converter = TextConverter.create(this);
	}

	public static TextMeta create() {
		return new TextMeta();
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.TEXT;
	}

	@Override
	public TextConverter getConverter() {
		return converter;
	}
}
