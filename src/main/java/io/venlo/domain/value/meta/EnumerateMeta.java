/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.DomainValueException;
import io.venlo.domain.value.EnumerateItem;
import io.venlo.domain.value.converter.EnumerateConverter;

public class EnumerateMeta implements ValueTypeMeta, Serializable {

	final EnumerateItem[] values;
	private final EnumerateConverter converter;

	private EnumerateMeta(final EnumerateItem[] values) {
		super();
		this.values = values;

		converter = EnumerateConverter.create(this);
	}

	public static EnumerateMeta create(final EnumerateItem... values) {

		return new EnumerateMeta(values);
	}

	public EnumerateItem valueOfCode(final String code) {

		for (final EnumerateItem item : values) {
			if (item.getCode().equals(code)) {
				return item;
			}
		}
		throw DomainValueException.create("Invalid enumerate code: " + code);
	}

	public EnumerateItem[] getValues() {
		return values;
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.STRING;
	}

	@Override
	public EnumerateConverter getConverter() {
		return converter;
	}
}
