/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value.meta;

import java.io.Serializable;

import io.venlo.domain.value.converter.IntegerConverter;

public class IntegerMeta implements ValueTypeMeta, Serializable {

	private final int minValue;
	private final int maxValue;

	private final IntegerConverter converter;

	private IntegerMeta(final int minValue, final int maxValue) {
		super();

		this.minValue = minValue;
		this.maxValue = maxValue;

		converter = IntegerConverter.create(this);
	}

	@Override
	public PrimitiveType getPrimitiveType() {
		return PrimitiveType.INTEGER;
	}

	public int getMinValue() {
		return minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public static IntegerMeta create(final int minValue, final int maxValue) {
		return new IntegerMeta(minValue, maxValue);
	}

	@Override
	public IntegerConverter getConverter() {
		return converter;
	}
}
