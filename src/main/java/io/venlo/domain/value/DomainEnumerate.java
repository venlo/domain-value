/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

public interface DomainEnumerate extends DomainValue<EnumerateItem> {

	String getCode();
}
