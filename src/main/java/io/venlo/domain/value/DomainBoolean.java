/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import org.eclipse.jdt.annotation.NonNull;

import io.venlo.domain.value.base.BaseBoolean;
import io.venlo.domain.value.converter.BooleanConverter;
import io.venlo.domain.value.meta.BooleanMeta;

public abstract class DomainBoolean<T> extends BaseBoolean {

	private static final BooleanMeta meta = BooleanMeta.create();
	private static final BooleanConverter converter = meta.getConverter();

	@Override
	@NonNull
	public String toString() {
		return converter.toString(getValue());
	}

	protected DomainBoolean(final Boolean value) {
		super(value);
	}

	protected abstract T createInstance(final Boolean value);

	public T negate() {
		return createInstance(!getValue());
	}

	public static BooleanMeta meta() {
		return meta;
	}

	public static BooleanConverter converter() {
		return converter;
	}
}
