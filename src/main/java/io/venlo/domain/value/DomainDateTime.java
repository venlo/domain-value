/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.time.LocalDateTime;

import io.venlo.domain.value.base.BaseDateTime;
import io.venlo.domain.value.meta.DateTimeMeta;

public abstract class DomainDateTime<T> extends BaseDateTime {

	protected DomainDateTime(final DateTimeMeta meta, final LocalDateTime value) {
		super(meta, value);
	}

	protected abstract T createInstance(final LocalDateTime value);

	public T plusSeconds(final int seconds) {
		return createInstance(getValue().plusSeconds(seconds));
	}

	public T minusSeconds(final int seconds) {
		return createInstance(getValue().minusSeconds(seconds));
	}

	public T plusMinutes(final int seconds) {
		return createInstance(getValue().plusMinutes(seconds));
	}

	public T minusMinutes(final int seconds) {
		return createInstance(getValue().minusMinutes(seconds));
	}

	public T plusHours(final int hours) {
		return createInstance(getValue().plusHours(hours));
	}

	public T minusHours(final int hours) {
		return createInstance(getValue().minusHours(hours));
	}

	public T plusDays(final int days) {
		return createInstance(getValue().plusDays(days));
	}

	public T minusDays(final int days) {
		return createInstance(getValue().minusDays(days));
	}
}
