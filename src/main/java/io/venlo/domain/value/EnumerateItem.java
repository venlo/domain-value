/*******************************************************************************
 * Copyright 2017, Van Kessel-IT, Netherlands,  All Rights Reserved.
 ******************************************************************************/

package io.venlo.domain.value;

import java.io.Serializable;

public class EnumerateItem implements Serializable {

	private final String value;
	private final String code;
	private final String labelCode;

	private EnumerateItem(final String value, final String code, final String labelCode) {
		super();
		this.value = value;
		this.code = code;
		this.labelCode = labelCode;
	}

	@Override
	public String toString() {
		return code;
	}

	public static EnumerateItem create(
			final String value, 
			final String code, 
			final String labelCode) {
		return new EnumerateItem(value, code, labelCode);
	}

	public String getValue() {
		return value;
	}

	public String getCode() {
		return code;
	}

	public String getLabelCode() {
		return labelCode;
	}

}
